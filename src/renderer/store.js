const Store = require('electron-store');
const store = new Store();

function getToDosById(id) {
    return store.get(id) ? store.get(id) : [];
}

function setToDo(id, item) {
    let obj = store.get(id) ? store.get(id) : {};
    let res = new Map(Object.entries(obj));
    const key = res.size ? res.size : 0;
    res.set(String(key), item);
    store.set(id, Array.from(res.values()));
}

function getTheme() {
    let theme = store.get("theme");
    if (!theme) {
        theme = "default-theme";
        store.set("theme", theme);
    }
    return theme;
}

function setTheme(theme) {
    store.set("theme", theme);
}

function deleteToDo(id, index) {
    let obj = store.get(id) ? store.get(id) : {};
    let res = new Map(Object.entries(obj));
    res.delete(String(index));
    store.set(id, Array.from(res.values()));
}

function setBgImg(path) {
    store.set("bgImg", path);
}

function getBgImg() {
    return store.get("bgImg") ? store.get("bgImg") : __dirname + "/img/bg2.jpg";
}

function setTimeFormat(format) {
    store.set("time-format", format);
}

function getTimeFormat() {
    if (!store.get("time-format")) {
        store.set("time-format", "12");
    }
    return store.get("time-format");
}

module.exports = {
    getToDosById,
    setToDo,
    deleteToDo,
    getTheme,
    setTheme,
    setBgImg,
    getBgImg,
    setTimeFormat,
    getTimeFormat
};