"use strict";

class Carousel {
    constructor({items, index, itemDecor, container, loop = false, visibleElementsNumber = 1}) {
        this.items = items;
        this.index = index;
        this.itemDecor = itemDecor;
        this.container = container;
        this.loop = loop;
        this.visibleElementsNumber = visibleElementsNumber;
        this.indexes = this.initIndexes();
        this.initContainer();
    }

    initIndexes() {
        let result = [];
        for (let i = this.index - this.visibleElementsNumber; i < this.index; i++) result.push(i);
        for (let i = this.index ; i <= this.index + this.visibleElementsNumber; i++) result.push(i);
        return result;
    }

    initContainer() {
        let result = "";
        for (let index of this.indexes) {
            result += this.itemDecor(this.items[index]);
        }
        this.container.innerHTML = result;
    }

    getNextIndex() {
        const len = this.indexes.length - 1;
        return this.indexes[len] === this.items.length - 1 ? 0 : this.indexes[len] + 1;
    }

    getPrevIndex() {
        return this.indexes[0] === 0 ? this.items.length - 1 : this.indexes[0] - 1;
    }

    setToEnd(item) {
        this.container.insertAdjacentHTML('beforeend', item);
        this.container.removeChild(this.container.children[0]);
    }

    setToStart(item) {
        this.container.insertAdjacentHTML('afterbegin', item);
        this.container.removeChild(this.container.children[this.container.children.length - 1]);
    }

    getPrevItem() {
        if (this.loop) {
            this.indexes.unshift(this.getPrevIndex());
            this.indexes.pop();
            this.index = this.indexes[(this.indexes.length - 1) / 2];
            this.setToStart(this.itemDecor(this.items[this.indexes[0]]));
        } else {
            this.setToStart(this.itemDecor(this.items[this.index - this.visibleElementsNumber]));
        }
    }

    getNextItem() {
        if (this.loop) {
            this.indexes.shift();
            this.indexes.push(this.getNextIndex());
            this.index = this.indexes[(this.indexes.length - 1) / 2];
            this.setToEnd(this.itemDecor(this.items[this.indexes[this.indexes.length - 1]]));
        } else {
            this.setToEnd(this.itemDecor(this.items[this.index + this.visibleElementsNumber]));
        }
    }
}

module.exports = {
    Carousel
};