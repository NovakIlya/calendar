"use strict";

const { setBgImg, getBgImg } = require("./store");

function preventDefaults (e) {
    e.preventDefault();
    e.stopPropagation();
}

function highlight(dropArea) {
    dropArea.classList.add('highlight')
}
function unhighlight(dropArea) {
    dropArea.classList.remove('highlight')
}

function handleDrop(e) {
    let dt = e.dataTransfer;
    let files = dt.files;
    handleFiles(files);
}

function handleFiles(files) {
    ([...files]).forEach(uploadFile)
}

function uploadFile(file) {
    setBgImg(file.path);
    showImg(file.path);
    changeImg();
}

function showImg() {
    let previewBox = document.querySelector(".preview-img");
    let child = document.createElement("img");
    child.src = getBgImg();
    previewBox.innerHTML = "";
    previewBox.appendChild(child);
}

function changeImg() {
    const wrapper = document.querySelector(".wrapper");
    if (wrapper.hasAttribute("default-theme")) {
        wrapper.style.backgroundImage = `url(${getBgImg()})`;
    }
}

function init() {
    let dropArea = document.getElementById("drop-area");

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
    });

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, () => highlight(dropArea), false)
    });
    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, () => unhighlight(dropArea), false)
    });

    dropArea.addEventListener('drop', handleDrop, false);

    showImg();
}

init();

module.exports = {
    changeImg
};