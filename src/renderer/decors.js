"use strict";

const { getTimeFormat } = require("./store");

function datesDecor(items, mod = 1) {
    if (mod) {
        const dates = items.map(item => item ? `<button class="button button_date" id="${item.id}">${item.date}</button>` : `<span></span>`).join("");
        return `<div class="months-dates carousel__item">${dates}</div>`;
    }
    return items.map(item => item ? `<button class="button button_date" id="${item.id}">${item.date}</button>` : `<span></span>`).join("");
}

function itemDecor(name) {
    return `<div class="carousel__item">${name}</div>`;
}

function dayDecor(name) {
    return `<span>${name}</span>`;
}

function formatAMPM(date) {
    let [ hours, minutes ] = date.split(":");
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? minutes : minutes;
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function toDosDecor(items) {
    items = Array.from(items);
    return items.map((item, index) => `
        <button class="button button_todo" data-index="${items.length < 2 ? 0 : index}" title="Delete">
            <span>${item.todo}</span>
            <br>
            <span class="time">Добавленно в <span>${getTimeFormat() === "12" ? formatAMPM(item.time) : item.time}</span></span>
        </button>
    `).join("");
}

module.exports = {
    datesDecor,
    itemDecor,
    dayDecor,
    toDosDecor
};