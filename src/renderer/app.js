'use strict';

const { ipcRenderer } = require('electron');
const prompt = require('electron-prompt');
const { Carousel } = require('./carousel');
const { datesDecor, itemDecor, dayDecor, toDosDecor } = require('./decors');
const { getToDosById, setToDo, deleteToDo, getTheme, getBgImg } = require("./store");

function getMonthDates(date) {
    const lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
    const firstMonthDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    let result = [];

    let i = 1;
    while (i <= firstMonthDay) {
        result.push(0);
        i++;
    }

    for (let i = 1; i <= lastDate; i++) {
        result.push({
            date: i,
            id: date.getFullYear() + '-' + date.getMonth() + '-' + i,
            hasEvent: false
        });
    }

    return result;

}

function getMonthsDatesArr(date) {
    const year = date.getFullYear();
    let result = [];

    for (let i = 0; i < 12; i++) {
        result.push(getMonthDates(new Date(year, i)));
    }

    return result;
}

function initYearsItems(year, k) { // year - текущая дата; k - количество дат
    year = year - (k - 1)/2;
    let result = [];
    for (let i = 0; i <= k; i++) {
        result.push(year);
        year++;
    }
    return result;
}

function initYearsCarousel(date) {
    const yearsCarousel = new Carousel({
        items: initYearsItems(date.getFullYear(), 9),
        index: 4,
        itemDecor: itemDecor,
        container: document.querySelector(".year-carousel"),
        visibleElementsNumber: 4
    });
    return yearsCarousel;
}

function initMonthsCarousel(date) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const monthsCarousel = new Carousel({
        items: months,
        index: date.getMonth(),
        itemDecor: itemDecor,
        container: document.querySelector(".months-carousel"),
        loop: true,
        visibleElementsNumber: 2,
    });
    return monthsCarousel;
}

function initMonthsDatesCarousel(date) {
    const monthsDatesCarousel = new Carousel({
        items: getMonthsDatesArr(date),
        index: date.getMonth(),
        itemDecor: datesDecor,
        container: document.querySelector(".months-dates-carousel"),
        loop: true
    });
    return monthsDatesCarousel;
}

function setInner(el, arr, decor = null) {
    for (let i = 0; i < arr.length; i++) {
        if (decor) {
            el.innerHTML += decor(arr[i]);
        } else {
            el.innerHTML += arr[i];
        }
    }
}

function getToDos(id) {
    const toDos = getToDosById(id);
    const dateInfo = document.querySelector(".date-info");
    dateInfo.dataset.dateId = id;
    dateInfo.innerHTML = toDosDecor(toDos);

    dateInfo.childNodes.forEach((item) => item.addEventListener("click", () => {
        deleteToDo(id, item.dataset.index);
        getToDos(id);
    }));
}

function addToDo() {
    prompt({
        label: "I have to ...",
        type: 'input',
        customStylesheet: __dirname + '/css/prompt.css',
        menuBarVisible: true,
        buttonLabels: {
            "ok": "Add",
        },
        transparent: true,
    }).then((v) => {
        if (v) {
            let date = new Date();
            let minutes = String(date.getMinutes()).length === 1 ? '0' + date.getMinutes() : date.getMinutes();
            let hours = date.getHours();
            let item = {
                todo: v,
                time: hours + ":" + minutes
            };
            let id = document.querySelector(".date-info").dataset.dateId;
            setToDo(id, item);
            getToDos(id);
        }
    }).catch(console.error);
}

function setDatesEvents(container) {
    for (const items of container.childNodes) {
        for (const item of items.children) {
            if (!item.hasEvent) {
                item.hasEvent = true;
                item.addEventListener("click", () => getToDos(item.id));
            }
        }
    }
}

function initDaysList() {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const daysListBox = document.querySelector(".days-list");
    setInner(daysListBox, days, dayDecor);
}

function getPrevYear(yearsCarousel) {
    yearsCarousel.items.pop();
    yearsCarousel.items.unshift(yearsCarousel.items[0] - 1);
}

function getNextYear(yearsCarousel) {
    yearsCarousel.items.shift();
    yearsCarousel.items.push(yearsCarousel.items[yearsCarousel.items.length - 1] + 1);
}

function prev(monthsCarousel, monthsDatesCarousel, yearsCarousel) {
    monthsCarousel.getPrevItem();
    monthsDatesCarousel.getPrevItem();
    setDatesEvents(monthsDatesCarousel.container);

    // ipcRenderer.send("go-to-settings");

    if (monthsDatesCarousel.index === 11) {
        up(yearsCarousel);
        monthsDatesCarousel.items = getMonthsDatesArr(new Date(yearsCarousel.items[yearsCarousel.indexes[4]], 0, 1));
        monthsDatesCarousel.container.childNodes[1].innerHTML = monthsDatesCarousel.itemDecor(monthsDatesCarousel.items[11], 0);
        monthsDatesCarousel.container.childNodes[0].innerHTML = monthsDatesCarousel.itemDecor(monthsDatesCarousel.items[10], 0);
        setDatesEvents(monthsDatesCarousel.container);
    }
}

function next(monthsCarousel, monthsDatesCarousel, yearsCarousel) {
    monthsCarousel.getNextItem();
    monthsDatesCarousel.getNextItem();
    setDatesEvents(monthsDatesCarousel.container);


    if (monthsDatesCarousel.index === 0) {
        bottom(yearsCarousel);
        monthsDatesCarousel.items = getMonthsDatesArr(new Date(yearsCarousel.items[yearsCarousel.indexes[4]], 0, 1));
        monthsDatesCarousel.container.childNodes[1].innerHTML = monthsDatesCarousel.itemDecor(monthsDatesCarousel.items[0], 0);
        monthsDatesCarousel.container.childNodes[2].innerHTML = monthsDatesCarousel.itemDecor(monthsDatesCarousel.items[1], 0);
        setDatesEvents(monthsDatesCarousel.container);
    }
}

function up(yearsCarousel, monthsDatesCarousel = null) {
    getPrevYear(yearsCarousel);
    yearsCarousel.getPrevItem();
    if (monthsDatesCarousel) {
        monthsDatesCarousel.items = getMonthsDatesArr(new Date(yearsCarousel.items[yearsCarousel.indexes[4]], 0, 1));
        monthsDatesCarousel.initContainer();
        setDatesEvents(monthsDatesCarousel.container);
    }


}

function bottom(yearsCarousel, monthsDatesCarousel = null) {
    getNextYear(yearsCarousel);
    yearsCarousel.getNextItem();
    if (monthsDatesCarousel) {
        monthsDatesCarousel.items = getMonthsDatesArr(new Date(yearsCarousel.items[yearsCarousel.indexes[4]], 0, 1));
        monthsDatesCarousel.initContainer();
        setDatesEvents(monthsDatesCarousel.container);
    }
}

function initTheme() {
    const link = document.getElementById("theme");
    link.href = `css/${getTheme()}.css`;
}

function initBg() {
    document.body.style.backgroundImage = `url(${getBgImg()})`;
}

function init() {
    let date = new Date();

    initDaysList();

    const monthsDatesCarousel = initMonthsDatesCarousel(date);
    const monthsCarousel = initMonthsCarousel(date);
    const yearsCarousel = initYearsCarousel(date);
    setDatesEvents(monthsDatesCarousel.container);

    const prevBtn = document.getElementById("lMn");
    const nextBtn = document.getElementById("rMn");

    getToDos(date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate());

    prevBtn.addEventListener("click", () => prev(monthsCarousel, monthsDatesCarousel, yearsCarousel));
    nextBtn.addEventListener("click", () => next(monthsCarousel, monthsDatesCarousel, yearsCarousel));

    const upBtn = document.getElementById("pYr");
    const bottomBtn = document.getElementById("nYr");

    upBtn.addEventListener("click", () => up(yearsCarousel, monthsDatesCarousel));
    bottomBtn.addEventListener("click", () => bottom(yearsCarousel, monthsDatesCarousel));

    const addBtn = document.getElementById("aBn");
    addBtn.addEventListener("click", () => addToDo());

    const settingsBtn = document.querySelector('.button_settings');
    settingsBtn.addEventListener("click", () => {
       ipcRenderer.send("go-to-settings");
    });

    initTheme();
    initBg();
}

init();