'use strict';
const { BrowserWindow } = require('electron');

const defaultProps = {
    width: 1000,
    height: 800,
    show: false,
    center: true,
    webPreferences: {
        nodeIntegration: true
    }
};

class Window extends BrowserWindow{
    constructor({file, ...windowSettings}) {
        super({...defaultProps, ...windowSettings});
        this.loadURL(`file://${__dirname}/${file}`);

        this.once('ready-to-show', () => {
            this.show();
        });
    }
}

module.exports = {
    Window
};