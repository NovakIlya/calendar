'use strict';

const { app, ipcMain } = require('electron');
const { Window } = require('./Window');


function main () {
    let win = new Window({
        file: 'renderer/index.html',
        title: "",
        icon: `${__dirname}/renderer/img/icon.png`,
    });
    win.setMenuBarVisibility(false);

    ipcMain.on("go-to-settings", () => {
       win.loadURL(`file://${__dirname}/renderer/settings.html`)
    });

    ipcMain.on("go-to-main", () => {
        win.loadURL(`file://${__dirname}/renderer/index.html`)
    });
}

app.on('ready', main);

app.on('window-all-closed', () => app.quit());